//Define a tela
const Tela=(valor,res)=>{
    return(
      <div style={cssTela}>
        <span style={cssTelaOper}>{valor}</span>
        <span style={cssTelaRes}>{res}</span>
      </div>
    )
  }

  //Define os botões
  const Btn=(label,onClick)=>{
    return(
      <button style={cssBtn} onClick={onClick}>{label}</button>
    )
  }

  //Define os botões laterais
  const BtnLat=(label,onClick)=>{
    return(
      <button style={cssBtnLat} onClick={onClick}>{label}</button>
    )
  }

  //Define o botão AC
  const BtnAc=(label,onClick)=>{
    return(
      <button style={cssBtnAc} onClick={onClick}>{label}</button>
    )
  }